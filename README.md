<p align="center">
    <h1 align="center">Yii 2 Advanced Template - Testing with GitLab</h1>
    <br>
</p>

Yii 2 Advanced Project Template [Yii 2 Advanced](https://github.com/yiisoft/yii2-app-advanced)

This test runs only the sample tests of advanced template [Yii 2 Advanced Testing](https://github.com/yiisoft/yii2-app-advanced/blob/master/docs/guide/start-testing.md) 

Follow the instrucctions for laravel only continuous-integration-with-gitlab (for this test), and change the following files
https://docs.gitlab.com/ee/ci/examples/laravel_with_gitlab_and_envoy/#continuous-integration-with-gitlab


Dockerfile  
---

```
# Docker file for Yii2 Advanced Template - Testing with GitLab
# Set the base image for subsequent instructions
FROM php:7.0

# Update packages
RUN apt-get update

# Install PHP and composer dependencies
RUN apt-get install -qq git curl wget unzip libmcrypt-dev libjpeg-dev libpng-dev libfreetype6-dev libbz2-dev

# Clear out the local repository of retrieved package files
RUN apt-get clean

# Install needed extensions
# Here you can install any other extension that you need during the test and deployment process
RUN docker-php-ext-install mcrypt pdo pdo_mysql zip opcache mysqli mbstring

RUN docker-php-ext-configure pdo_mysql --with-pdo-mysql=mysqlnd

RUN docker-php-ext-configure gd --with-gd --with-freetype-dir=/usr/include/ --with-png-dir=/usr/include/ --with-jpeg-dir=/usr/include/ --with-freetype-dir=/usr/include/freetype2

RUN docker-php-ext-install gd

# Install Composer
RUN curl --silent --show-error https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
```

.gitlab-ci.yml  
---
Get the full image from GitLab - Registry tab
```
image: registry.gitlab.com/<USERNAME>/<repo>:latest

services:
  - mysql:5.7

variables:
  MYSQL_DATABASE: yii2advanced_test
  MYSQL_ROOT_PASSWORD: tester
  DB_HOST: mysql
  DB_USERNAME: root

before_script:
  - composer global require "fxp/composer-asset-plugin:^1.2.0"
  - ./init --env=Development --overwrite=All
  - composer update
  - php requirements.php

stages:
  - test

unit_test:
  stage: test
  script:
    - ./yii_test migrate --interactive=0
    - vendor/bin/codecept build
    - vendor/bin/codecept run
```

For Dev Environments
---
Change file environments/dev/common/config/test-local.php 
The change here is the host, from localhost to mysql and the password configure before

test-local.php  
---
```
<?php
return yii\helpers\ArrayHelper::merge(
    require __DIR__ . '/main.php',
    require __DIR__ . '/main-local.php',
    require __DIR__ . '/test.php',
    [
        'components' => [
            'db' => [
                'class' => 'yii\db\Connection',
                'dsn' => 'mysql:host=mysql;dbname=yii2advanced_test',
                'username' => 'root',
                'password' => 'tester',
                'charset' => 'utf8',
            ]
        ],
    ]
);

```